#!/usr/bin/env python3

import unittest
import sums

class test_sums(unittest.TestCase):

  def test_add(self):
    x = sums.sums()
    assert x.add(3) == 3, "The add method failed to add"

if __name__ == "__main__":
  unittest.main()
