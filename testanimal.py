#!/usr/bin/env python3

import unittest
import animals

class test_animals(unittest.TestCase):

    def test_eat(self):
        x = animals.animal()
        x.eat(3)
        assert x.weight == 81.5, "Weight isn't correct"

    def test_run(self):
        x = animals.animal()
        x.run(1)
        assert x.weight == 79.9, "I am a failure"

if __name__ == "__main__":
  unittest.main()
