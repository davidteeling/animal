#!/usr/bin/env python3

class animal:
    def __init__(self):
        self.legs=4
        self.weight=80
        self.height=120
        self.age=15
        self.colour="Brown"
        self.furORskin="Fur"

    def eat(self,times):
        self.weight = self.weight+(times*.5)
        self.height = self.height+(times*.5)

    def birthday(self):
        self.age=self.age+1

    def showweight(self):
        print(self.weight)

    def showheight(self):
        print(self.height)

    def run(self,times):
        self.weight = self.weight-(times*.1)
        
if __name__ == "__main__":
    x = animal()
    x.eat(3)
    print(x.showweight())
